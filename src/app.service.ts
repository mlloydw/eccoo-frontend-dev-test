import { Injectable, NotImplementedException } from '@nestjs/common';
import { MirrorQueryType } from './mirror-query.type';

export type BackendData = {
  name: string;
  role?: string;
};

@Injectable()
export class AppService {
  /**
   * This returns a *Promise* to mimic a call to a backend service.
   */
  getBackendData(): Promise<BackendData[]> {
    return Promise.resolve([
      {
        name: 'Person 1',
        role: 'Care Co-ordinator',
      },
      {
        name: 'Person 2',
      }
    ]);
  }

  /**
   * Converts a query object into a string representation.
   * 
   * @param query An object to be queried
   * @returns     A string representation of the object
   */
  mirrorQuery(query: MirrorQueryType): string {
    throw new NotImplementedException();
  }
}
